= Generische Programmierung (Generics)
Dominikus Herzberg, Christopher Schölzel
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:stylesheet: italian-pop.asciidoc.css
:source-highlighter: coderay
:sourcedir: code/10

include::prelude.adoc[]

Die Aufgaben in diesem Kapitel üben die sogenannte generische Programmierung ein.

.Mehr ist besser
****
Wenn Sie noch ein paar Idee für Aufgaben haben, machen Sie uns Vorschläge!
****

== Generischer Stack

Sie haben eventuell bereits generische Klassen mit Typparametern in Form der Collection-API kennengelernt und verwendet, aber wie funktioniert das eigentlich? Wie kann man eine eigene generische Klasse definieren?

Genau das sollen Sie in dieser Übung lernen. Dazu schauen wir uns zunächst Auszüge aus der Definition der Klasse `ArrayList` an. Auslassungen sind dabei mit dem Ausdruck `[...]` gekennzeichnet.

[source,java]
----
include::{sourcedir}/arrayList.java[]
----
<1> Hier wird definiert, dass zu der Klasse `ArrayList` ein Typparameter namens `E` gehört. Wird die Klasse einfach nur als `ArrayList` verwendet, verhält sie sich so als würde man jedes `E` durch ein `Object` austauschen. Eine `ArrayList<Integer>` dagegen arbeitet mit `Integer`-Objekten, wo in der Definition ein `E` vorkommt.
<2> Der Parameter `E` kann überall auftauchen wo Klassennamen verwendet werden: Als Typ einer Variable oder eines Parameters oder als Rückgabetyp. Natürlich kann man von einem Objekt von einem unbekannten bzw. beliebigen Typ `E` auch nur Methoden aufrufen, die _jedes_ Objekt in Java besitzt - also alles was in der Klasse `Objekt` definiert ist. Für die `ArrayList` reicht das völlig, da mit den Objekten ja nichts anderes geschieht als dass sie in ein Array gesteckt oder wieder aus diesem herausgeholt werden.
<3> Es wirkt etwas seltsam, dass das interne Array nicht vom Typ `E[]` statt vom Typ `Object[]` ist. Es spricht nichts dagegen eine Variable oder ein Feld vom Typ `E[]` zu definieren, aber in Java ist das Erzeugen von generischen Arrays etwas komplizierter. Das hat damit zu tun, dass Typparameter vom Java-Compiler entfernt und durch den Typ `Object` (mit entsprechenden Typcasts an den relevanten Stellen) ersetzt werden.

Für ihre Klasse `Stakk` bzw. `Stapel` aus den vorherigen Übungsblättern ist die Situation sogar noch etwas einfacher. Schaffen Sie es analog zu dem Beispiel der `ArrayList` auch einen generischen `Stakk` zu bauen, so dass z.B. die folgenden Aufrufe möglich werden?

----
Stakk<String> stringStack = new Stakk<String>("First");
stringStack.push("Second");
Stakk<Double> doubleStack = new Stakk<Double>(Double.NaN);
doubleStack.push(3.5);
double d = doubleStack.pop();
----

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/genericStack.java[]
----
<1> Hier haben wir einen Trick verwendet um unser generisches Array zu erzeugen, der auch in der Methode `toArray` von `ArrayList` zu sehen ist: Wir verlangen vom Benutzer, dass er uns ein Array des korrekten Typs übergibt, damit wir und mit `Arrays.newInstance` ein Array vom gleichen Typ erzeugen können. Man kann die Implementierung noch etwas effizienter machen indem man direkt das übergebene Array zum speichern der Werte verwendet, falls es groß genug ist.
<2> Auch Methoden können eigene Typparameter besitzen. Bei dieser statischen Methode ist das nötig, da bei dem Aufruf von `Stakk.fromArray` kein Objekt vom Typ `Stakk` beteiligt ist von dem man den Typparameter ableiten könnte.
include::postDetails.adoc[]

== Turing-Maschine

Der Mathematiker und Informatik-Pionier Alan Turing erfand in jungen Jahren die Turing-Maschine, die allerdings lediglich als theoretisches Konstrukt gedacht war. Turing benötigte ein einfaches Rechenmodell, mit dem er über das Wesen und die Möglichkeiten maschinellen Rechnens nachdenken konnte. Er tat das zu einer Zeit, bevor es einen "echten" Computer gab. Allerdings lag die Idee einer vollautomatischen Rechenmaschine in der Luft. Man sieht es der Idee Turings an, sie kombiniert die Idee der Schreibmaschine mit einem Bandgerät, das an ein Tonband erinnert, und lässt beides durch einen kleinen Steuerapparat koordiniert arbeiten.

Finden Sie anhand des Codes heraus, wie die Turing-Maschine funktioniert. Vermutlich werden Ihnen die Erläuterungen auf https://de.wikipedia.org/wiki/Turingmaschine[Wikipedia] eine Hilfe sein.

[source,java]
----
include::{sourcedir}/turing.java[]
----