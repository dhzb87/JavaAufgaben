import java.util.regex.Pattern;
import java.util.regex.Matcher;

class EMail implements SimpleEMail {
  private static final Pattern simpleMail = Pattern.compile(
    "[a-z]+(\\.[a-z]+)*@[a-z0-9\\-]+(\\.[a-z0-9\\-]+)*");
  String domain;
  String[] names;
  EMail(String email) {
    Matcher m = simpleMail.matcher(email);
    if(!m.matches()) throw new IllegalArgumentException();
    String[] split = email.split("@");
    domain = split[1];
    names = split[0].split("\\.");
  }
  public String[] getNames() {
    return names;
  }
  public String getDomain() {
    return domain;
  }
}