class Packet {
  String type;
  float price;
  Packet(int h, int w, int d, float weight) {
    if (h < 0 || w < 0 || d < 0 || weight < 0) {
      throw new IllegalArgumentException(
        "Weight and measures must be positive!");
    }
    // sort measures for determining typ
    int[] dim = new int[]{h, w, d};
    Arrays.sort(dim);
    // beginning from the smallest type, determine the first type that fits
    if (dim[2] <= 30 && dim[1] <= 30 && dim[0] <= 15 && weight <= 1) {
      type = "Päckchen";
      price = 4;
    } else if (dim[2] <= 60 && dim[1] <= 30 && dim[0] <= 15 && weight <= 2) {
      type = "Maxi-Päckchen";
      price = 4.5f;
    } else if (dim[2] <= 120 && dim[1] <= 60 && dim[0] <= 60 && weight <= 5) {
      type = "Paket";
      price = 6.99f;
    } else {
      throw new IllegalArgumentException("Packet too large or too heavy!");
    }
  }
  String getType() {
    return type;
  }
  float getPrice() {
    return price;
  }
  static float sumPrices(List<Packet> packets) {
    float sum = 0;
    for(Packet p : packets) {
      sum += p.getPrice();
    }
    return sum;
  }
}
