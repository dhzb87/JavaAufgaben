= 12. Woche: OO-Aufgaben
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
V0.2, 2017-01-18
:toc:
:icons: font
:solution:
:source-highlighter: coderay
:sourcedir: ./code/
:imagedir: ./images

== Beispiele für Fragen und Korrektur-Aufgaben

=== Reguläre Ausdrücke (4 Punkte)

Zu welchen der folgenden Zeichenketten passt der reguläre Ausdruck: `\d+\. [A-Z][a-z]+ \d\d\d\d`
// Kein Satzendepunkt nach regulörem Ausdruck, ist missverständlich

* [ ] `1. Juli 2014`
* [ ] `7. may 2000`
* [ ] `200. Xx 0000`
* [ ] `1. Januar 1970`
* [ ] `2. DEZ 1998`
* [ ] `68748967. Foobardoobar 0001`
* [ ] `1. D 17`
* [ ] `7th May 1999`

ifdef::solution[]
.Lösung
* [x] `1. Juli 2014`
* [ ] `7. may 2000`
* [x] `200. Xx 0000`
* [x] `1. Januar 1970`
* [ ] `2. DEZ 1998`
* [x] `68748967. Foobardoobar 0001`
* [ ] `1. D 17`
* [ ] `7th May 1999`
endif::solution[]

=== Call-Stack (8 Punkte)

Betrachten Sie das folgende Java-Programm. Welche Ausgabe erzeugt es bei der Ausführung mit der JShell?

[source, java]
----
include::{sourcedir}/12/CallStack.java[]
----

== Beispiele für Kurz-Aufgaben

=== Swappable testen

Zur Aufgabe aus der letzten Woche: Schreiben Sie eine Klasse, die das Interface `Swappable` implementiert, um ihren Code für die Methode `shuffle` zu testen.

Zur Erinnerung hier noch einmal das Interface:

[source,java]
----
include::{sourcedir}/11/Swappable.java[]
----

#### Variante 1 (6 Punkte)
Ihre Klasse darf kein `extends` verwenden. Sie dürfen nur die Methoden des Interfaces und einen Konstruktor implementieren - keine weiteren Methoden.

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/12/SwappableList1.java[]
----
endif::solution[]

#### Variante 2 (4 Punkte)
Ihre Klasse darf keine Objekte erzeugen oder als Argument im Konstruktor übernehmen. Dieses Mal dürfen Sie sogar nur noch die Methode `swap` implementieren, keine weiteren Methoden und auch keine Konstruktoren.

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/12/SwappableList2.java[]
----
endif::solution[]

////
=== Vererbung statt Interface
// TODO überarbeiten
Aufgabe letzte Woche: Das Interface extrahieren. Stattdessen: Die Oberklasse extrahieren.
////

=== Typhierarchie (10 Punkte)

Implementieren Sie Java-Klassen und/oder -Interfaces (ohne Inhalt), die die folgende Typhierarchie abbilden. Dabei bedeutet ein Pfeil immer "ist ein" bzw. "ist Subtyp von".

image::{imagedir}/Hierarchy.png[]

=== Iteratoren und Arrays (10 Punkte)

Schreiben Sie eine Klasse `ArrayIterator<E>`, die beim Konstruktoraufruf ein Array vom Typ `E[]` übernimmt und das Interface `Iterator<E>` implementiert. Beim Aufruf von `remove` soll die Klasse einfach eine `UnsupportedOperationException` werfen.

[source,java]
----
include::{sourcedir}/12/Iterator.java[]
----

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/12/ArrayIterator.java[]
----
endif::solution[]

=== Elemente zählen (6 Punkte)

Schreiben Sie eine generische Methode `count`, die ein Argument vom Typ `Collection<E>` übernimmt und eine Abbildung vom Typ `Map<E,Integer>` zurückgibt. Die Schlüssel dieser Abbildung sind die Elemente der übergebenen Kollektion, denen als Wert die Anzahl ihrer Vorkommen in der Kollektion zugewiesen wird.

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/12/hist.java[]
----
endif::solution[]

== Skizze für Programmieraufgaben in Klausur

=== Paketpreise (20 Punkte)

Schreiben Sie eine Klasse `Packet`, die ein Postpaket darstellt. Die Klasse soll folgende Methoden besitzen:

* Einen Konstruktor, der das Gewicht sowie die Breite, Höhe und Tiefe des Pakets als Ganzzahlen übernimmt. Wenn das Paket größer oder schwerer ist als maximal erlaubt (s.u.) oder die Argumente aus anderen Gründen nicht sinnvoll sind, soll der Konstruktor eine `IllegalArgumentException` werfen.
* Die Methode `getType()`, die den Typ des Paketes entsprechend der folgenden Bestimmungen für Größe und Gewicht als Zeichenkette zurückgibt.
+
[cols=3,options="header"]
|===
|Typ
|max. Maße
|max. Gewicht
|Päckchen
|30x30x15 cm
|1 kg
|Maxi-Päckchen
|60x30x15 cm
|2 kg
|Paket
|120x60x60 cm
|5 kg
|===
+
Denken Sie dabei unbedingt daran, dass die Reihenfolge von Höhe, Tiefe und Breite austauschbar ist. Ein Paket mit den Maßen `30x45x15` muss also genauso behandelt werden wie ein Paket mit den Maßen `15x30x45` oder `45x30x15`.
* Die Methode `getPrice()`, die die Portokosten für das Paket nach seinem Typ berechnet.
+
[cols=2,options="header"]
|===
|Typ
|Preis
|Päckchen
|4 €
|Maxi-Päckchen
|4,5 €
|Paket
|6,99 €
|===
* Die statische Methode `sumPrices`, die eine Liste von `Packet`-Objekten übernimmt und deren Portokosten zusammenrechnet.

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/12/Packet.java[]
----
endif::solution[]

////
Ideen:
Distinct (duplikate entfernen) bzw. containsDuplicates
Mittelpunkt einer Linie

Themen:
Variablen
Operatoren
Verzweigungen
Schleifen
Arrays
Zeichenketten
Datum und Uhrzeit
Methoden
Exceptions
Klassen
Vererbung und Schnittstellen
Generische Klassen und Methoden
Collections
Dateien und Verzeichnisse
Javadoc
Pakete und Bibliotheken
////

== Knobelaufgaben (keine Klausuraufgaben)

=== Timer

[source, java]
----
try (Timer t = new Timer("Stuff")) {
  long sum = 0;
  for(int i = 0; i < Integer.MAX_VALUE; i++) {
    sum += i;
  }
  printf("sum = %d\n", sum);
}
----

Implementieren Sie die Klasse `Timer`, so dass bei der Ausführung des oben stehenden Codes eine Ausgabe wie die folgende produziert wird:

----
sum = 2305843005992468481
Stuff took 693100773 ns
----

ifdef::solution[]
.Lösung
[source,java]
----
include::{sourcedir}/12/Timer.java[]
----
endif::solution[]

=== Regex-Kreuzworträtsel

Lösen Sie das folgende Regex-Kreuzworträtsel. In jede Zelle muss ein Buchstabe eingetragen werden, so dass die Zeilen und Spalten auf die am Rand angegebenen regulären Ausdrücke passen.

[cols="h,1,1,1",options="header"]
|===
h| Regex\Regex
h| [JUNDT]*
h| APA\|OPI\|OLK
h| (NA\|FE\|HE)[CV]
h|[DEF][MNO]*
|
|
|
|[^DJNU]P[ABC]
|
|
|
|[ICAN]*
|
|
|
|===

[TIP]
====
Wenn sie Spaß an dieser Aufgabe haben, schauen Sie sich die Webseite https://regexcrossword.com/ an. Dort gibt es noch viel mehr (und viel schwierigere) Regex-Kreuzworträtsel.
====


=== Permutationen

Schreiben Sie ein vollständiges Java-Programm, das eine Zeichenkette von der Kommandozeile übernimmt und nacheinander alle möglichen Anordnungen der darin enthaltenen Zeichen ausgibt.

Beispiele:
----
C:\temp>java Permutator Bla
Bla
Bal
lBa
laB
aBl
alB
C:\temp>java Permutator Foo
Foo
oFo
ooF
----
