class Point {
  int x;
  int y;
  Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
  public boolean equals(Object other) {
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Point)) return false; // <1>
    Point otherPoint = (Point) other;            // <2>
    return otherPoint.x == this.x && otherPoint.y == this.y;
  }
  public int hashCode() {
    return Objects.hash(x, y);                   // <3>
  }
}

class Car {
  double tachometer;
  Car(double tachometer) {
    this.tachometer = tachometer;
  }
  public boolean equals(Object other) {
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Car)) return false; // <1>
    Car otherCar = (Car) other;                // <2>
    return otherCar.tachometer == this.tachometer;
  }
  public int hashCode() {
    return Objects.hash(tachometer);           // <3>
  }
}


class Fraction {
  private final long numerator;
  private final long denominator;
  Fraction(long numerator) {
    this(numerator, 1L);
  }
  Fraction(long numerator, long denominator) {
    this.numerator = numerator;
    this.denominator = denominator;
  }
  public boolean equals(Object other) {
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Fraction)) return false; // <1>
    Fraction otherFrac = (Fraction) other;          // <2>
    return otherFrac.numerator == this.numerator
        && otherFrac.denominator == this.denominator;
  }
  public int hashCode() {
    return Objects.hash(numerator, denominator);    // <3>
  }
}

assert !new Fraction(1,2).equals(new Car(123.0));
assert new Fraction(1,2).equals(new Fraction(1,2));
assert new Fraction(1,2).hashCode() == new Fraction(1,2).hashCode();
assert new Fraction(2).equals(new Fraction(2,1));
assert new Fraction(2).hashCode() == new Fraction(2, 1).hashCode();
assert !new Car(12).equals(new Car(12.1));
assert new Car(12).equals(new Car(12.0));
assert new Car(2).hashCode() == new Car(2).hashCode();
assert !new Point(3,4).equals(new Point(4,3));
assert new Point(3,4).equals(new Point(3,4));
assert new Point(3,4).hashCode() == new Point(3,4).hashCode();
