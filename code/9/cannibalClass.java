class NomNomException extends Exception {
  NomNomException(String msg) {
    super(msg);
  }
}

class CannibalGame implements CannibalInterface {
  int[] cannibals = {3, 0};
  int[] missionaries = {3, 0};
  int side = 0;
  public void moveBoat(int c, int m) throws NomNomException {
    if (isIllegalMove(c, m)) {
      throw new IllegalArgumentException("Illegal move!");
    }
    int otherSide = 1 - side;
    cannibals[side]      -= c;
    cannibals[otherSide] += c;
    missionaries[side]      -= m;
    missionaries[otherSide] += m;
    side = otherSide;
    checkForNomNom();
  }
  private boolean isIllegalMove(int c, int m) {               // <1>
    return c < 0 
        || m < 0
        || c + m > 2
        || c == 0 && m == 0
        || cannibals[side] < c
        || missionaries[side] < m;
  }
  private void checkForNomNom() throws NomNomException {      // <1>
    if (   cannibals[0] > missionaries[0] && missionaries[0] > 0
        || cannibals[1] > missionaries[1] && missionaries[1] > 0) {
      throw new NomNomException("Oh no! Missionaries were eaten!");
    }
  }
}

// Lösung für das Rätsel:
CannibalInterface g = new CannibalGame();
g.moveBoat(2,0);
g.moveBoat(1,0);
g.moveBoat(2,0);
g.moveBoat(1,0);
g.moveBoat(0,2);
g.moveBoat(1,1);
g.moveBoat(0,2);
g.moveBoat(1,0);
g.moveBoat(2,0);
g.moveBoat(1,0);
g.moveBoat(2,0);
