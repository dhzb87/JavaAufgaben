int[][] field = {
    {1,  2,  3,  4},
    {5,  6, 42,  8},
    {9, 10, 11, 12}
}
outer: for(int y = 0; y < field.length; y++) {
    for(int x = 0; x < field[y].length; x++) {
        if (field[y][x] == 42) break outer;
        field[y][x] = 0;
    }
}