int fib(int n){
  if(n == 1)
    return 1;
  else if (n == 2)
    return 1;
  else {
    return fib(n-1) + fib(n-2);
  }

}


int fibIter(int n) {
  int f0 = 1, f1 = 1;
  for(int i = 1; i < n; ++i) {

    int oldF0 = f0;
    f0 = f1;
    f1 = oldF0 + f1;
}
return f1;
}
