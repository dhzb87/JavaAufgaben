import java.util.Map;
import java.util.HashMap;

void goShopping() {
  int count = 0;
  Map<String, Double> articles = new HashMap<>();
  articles.put("Chips", 1.5);
  articles.put("Fertigpizza", 2.0);
  articles.put("Jede Menge Bier", 10.0);
  double sum = 0;
  for (Double d: articles.values())
    sum += d;

  for (Map.Entry<String, Double> e: articles.entrySet()) {
    printf("%20s: %.2f\n", e.getKey(), e.getValue());
  }
  printf("%20s: %.2f", "SUMME", sum);
}


String bestStudent(Map<String, Double> students) {
  double best = Double.NEGATIVE_INFINITY;
  String bestName = "";
  for(String cur: studends.keySet()) {
    if(students.get(cur) > best) {
      best = students.get(cur);
      bestName = cur;
    }
  }
  return bestName;
}
