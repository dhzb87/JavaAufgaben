class Car {
    double tachometer;

    static double mileage(double fuel, double kilometers) {
        return fuel / kilometers * 100.0;
    }

    void setZero() {
        tachometer = 0;
    }

    void drive(double kmh, double minutes) {
        tachometer += kmh * minutes/60;
    }
 }
