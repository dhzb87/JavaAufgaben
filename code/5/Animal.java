class Animal{

  private String name;
  private String species;
  private String sound;
  private int age;


  public Animal(){
    this.name = "Freddy";
    this.species = "Affe";
    this.sound = "Uhuhuhu";
    this.age = 4;
  }

  public Animal(String name, String species, String sound, int age){
    this.name = name;
    this.species = species;
    this.sound = sound;
    this.age = age;
  }

  public int getAge(){
    return age;
  }

  public String makeSound(int quantity){
    String res = "";
    for(int i = 0; i < quantity; i++){
      res = res + this.sound;
    }
    res = res + "\n";
    return res;
  }

  public String getIdentity(){
    return "Ich heiße " + this.name + " und bin ein/eine " + this.species + ".";
  }

  public static int countAllAges(Animal[] animals){
    int res = 0;
    for (int i = 0; i < animals.length; i++){
      if(animals[i] != null){
        res = res + animals[i].getAge();
      }
    }
    return res;
  }

}
