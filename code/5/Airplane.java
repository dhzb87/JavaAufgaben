class Airplane{

  String name;
  String modell;
  int maxSpeed;


  public Airplane(){
    this.name = "Oldenburg";
    this.modell = "A380";
    this.maxSpeed = 500;
  }

  public Airplane(String name, String modell, int maxSpeed ){
    this.name = name;
    this.modell = modell;
    this.maxSpeed = maxSpeed;
  }

  public void fly(int speed){
    if(this.maxSpeed >= speed){
        System.out.printf("Flugzeug %s fliegt %d mph.", this.name, speed);
    } else{
        System.out.printf("Zu schnell! Flugzeug %s darf nur maximal %d mph fliegen.", this.name, this.maxSpeed);
    }
  }

  public static void flyAll(Airplane[] planes, int speed){
    for (int i = 0; i < planes.length; i++){
      if(planes[i] != null){
        planes[i].fly(speed);
        System.out.printf("\n");
      }
    }
  }

}
